/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

    package mygame;

/**
 *
 * @author edwardcrupi
 */
public class PlayerData implements java.io.Serializable {
    
    private int      index;
    public float    x,y,z;
    public float    rotation, speed;
    private boolean  crash = false;
    
    public PlayerData(int newIndex){
        index = newIndex;
        speed = 0;
        x = -5f+index*.3f;
        y = -3f;
        z = .1f;
    }
    
    public PlayerData(float newSpeed,
                               float newx, float newy, float newz,
                               float newRotation, int newIndex)   {
        index = newIndex;
        speed = newSpeed;
        x = newx;
        y = newy;
        z= newz;
        rotation = newRotation;
    }
    
    public void update(float newSpeed,
                               float newx, float newy, float newz,
                               float newRotation)   {
        speed = newSpeed;
        x = newx;
        y = newy;
        z= newz;
        rotation = newRotation;
    }
    
    public boolean isCrashed(){
        return crash;
    }

    public int getIndex(){
        return index;
    }
    
    public void crash(){
        crash = true;
    }

}