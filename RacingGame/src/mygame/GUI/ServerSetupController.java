package mygame.GUI;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.RadioButtonGroupStateChangedEvent;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;
import mygame.Main;

public class ServerSetupController extends AbstractAppState implements ScreenController {

  private Application app;
  private AppStateManager stateManager;
  private Nifty nifty;
  private Screen screen;
  private final String[] radio = {"radioButton1", "radioButton0", "radioButton4",
      "radioButton4", "radioButton2"
  };
  private Main game;  
  public ServerSetupController(Main newGame) {
      game = newGame;
  }
  
  public void bind(Nifty nifty, Screen screen) {
        this.nifty = nifty;
        this.screen = screen;
  }
  
  public void onStartScreen() {
  }

  public void onEndScreen() {
  }
  
  public void selectOne(){
      game.setPlayerNumber(1);
  }
  
  public void quit(){
     /*if(RadioButtonGroupStateChangedEvent.getSelectedId().equals(radio[0])) {
          game.setPlayerNumber(1);
      }
     if(RadioButtonGroupStateChangedEvent.getSelectedId().equals(radio[1])) {
          game.playerNumber = 2;
     }
     if(RadioButtonGroupStateChangedEvent.getSelectedId().equals(radio[2])) {
          game.playerNumber = 3;
          }
     if(RadioButtonGroupStateChangedEvent.getSelectedId().equals(radio[3])) {
          game.playerNumber = 4;
               }
     if(RadioButtonGroupStateChangedEvent.getSelectedRadioButton().equals(radio[0])) {
          game.playerNumber = 5;
     }*/     
     Main.isRunning = true;
     nifty.exit();
  }
    
  @Override
  public void initialize(AppStateManager stateManager, Application app) {
    this.app = app;
    this.stateManager = stateManager;
  }
}