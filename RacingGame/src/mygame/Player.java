/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

/**
 *
 * @author edwardcrupi
 */
public class Player extends OtherPlayer implements java.io.Serializable {
    
    private Spatial mesh;
    private Node playerNode;
    //private RigidBodyControl phy;
    private Vector3f position, velocity;
    private float speed = 0, angle = 0;
    private ColorRGBA color;
    private AssetManager assetManager;
    private InputManager inputManager;
    private static final float MAXSPEED = .005f;
    public static final Quaternion PITCH090 = 
                new Quaternion().fromAngleAxis(FastMath.PI/2,
                new Vector3f(1,0,0));
    private PlayerData playerData;
    private int index;
    
    private ColorRGBA[] playerColor = {ColorRGBA.Blue,
                                   ColorRGBA.Green,
                                   ColorRGBA.Pink,
                                   ColorRGBA.Cyan,
                                   ColorRGBA.White
                                  };
    
    public Player(float x, float y, float z, int newIndex, AssetManager newAM, InputManager newIM)
    {
        super(x,y,z,newIndex,newAM);
        position = new Vector3f(x, y, z);
        velocity = Vector3f.ZERO;
        playerData = new PlayerData(speed, position.x, position.y, position.z, angle, newIndex);
        index = newIndex;
        color = playerColor[newIndex];
        assetManager = newAM;
        inputManager = newIM;
        
        //Initialize Player Mesh
        mesh = assetManager.loadModel(
            "Models/newcar.j3o");
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", color);
        mesh.setMaterial(mat);

        //Get model in right shape and position.        
        mesh.scale(2f);
        mesh.setLocalRotation(PITCH090);
        mesh.rotate(0, FastMath.PI, 0);
        initKeys();
    };
    
    public void initKeys(){
        inputManager.addMapping("Left", new KeyTrigger(KeyInput.KEY_LEFT));
        inputManager.addMapping("Right", new KeyTrigger(KeyInput.KEY_RIGHT));
        inputManager.addMapping("Up", new KeyTrigger(KeyInput.KEY_UP));
        inputManager.addMapping("Down", new KeyTrigger(KeyInput.KEY_DOWN));
        
        inputManager.addMapping("Left2", new KeyTrigger(KeyInput.KEY_F));
        inputManager.addMapping("Right2", new KeyTrigger(KeyInput.KEY_H));
        inputManager.addMapping("Up2", new KeyTrigger(KeyInput.KEY_T));
        inputManager.addMapping("Down2", new KeyTrigger(KeyInput.KEY_G));
        inputManager.addMapping("Camera1", new KeyTrigger(KeyInput.KEY_SPACE));
    //Add the names to the action listener
        inputManager.addListener(analogListener, "Left", "Right", "Up", "Down",
                                             "Left2", "Right2", "Up2", "Down2");
    };
    
    private AnalogListener analogListener = new AnalogListener()
    {
        public void onAnalog(String name, float value, float tpf)
        {
            if(Main.isRunning)
            {
                float angleDif = 1f;
                if(name.equals("Right")){
                    angle += angleDif*speed;
                    mesh.rotate(0, -angleDif*speed, 0);
                }
                if(name.equals("Left")){
                    angle -= angleDif*speed;
                    mesh.rotate(0, angleDif*speed, 0);
                    //System.out.println("Angle = " + angle);
                }
                if(name.equals("Up")){
                        speed += .0001;
                        //System.out.println("Speed = " + speed);
                }
                if(name.equals("Down")){
                        speed -= .0001;
                }
        
            }
            else {
                System.out.println("Press P to unpause.");
            }
        }
    };
    
    public void movePlayer()
    {
        velocity = new Vector3f(
                    (float)Math.sin(angle)*speed,
                    (float)Math.cos(angle)*speed,
                    0);
       if(!blocked(position.add(velocity))){
            position = position.add(velocity);
            mesh.setLocalTranslation(position);
       }
       else speed = 0;
    };
    
    public PlayerData getData(){
        return playerData;
    }
    
    public void Update()
    {
        if(playerData.isCrashed()){
            speed = 0;
        }
        playerData.update(speed, position.x, position.y, position.z, angle);
        //Slow down if not accelerating
        if(speed > 0)
        {
            speed -= .000005;
        }
        if(speed < 0)
        {
            speed += .000005;
        }
        if(speed > MAXSPEED){
            speed = MAXSPEED;
        }
        if(speed < -MAXSPEED){
            speed = -MAXSPEED;
        }
        
        //Border collision detection
        if(position.x > 5.25f){
            position.x = 5.25f;
            speed = 0;
        }
        if(position.y < -4){

            position.y = -4f;
            speed = 0;
        }
        if(position.x < -5.25){
            position.x = -5.25f;
            speed = 0;
        }
        if(position.y > 4){
            position.y = 4f;
            speed = 0;
        }
        /*if(position.x > -3 && position.x < 3  && position.y < 2 && position.y > -2 )
        {
            if(position.x > 0 && position.y > 0)
                if(position.x > 3)
                    position.x = 3f;
                else position.y = 2f;
            else if(position.x > 0 && position.y < 0)
                if(position.x > 3)
                    position.x = 3f;
                else position.y = -2f;
            else if(position.x < 0 && position.y < 0)
                if(position.x < -3)
                    position.x = -3f;
                else position.y = -2f;
            else if(position.x < 0 && position.y > 0)
                if(position.x < -3)
                    position.x = -3f;
                else position.y = 2f;
            speed = 0;
        }*/
        //Move the player
        movePlayer();
    }
    
    public boolean blocked(Vector3f next){
        if(FastMath.abs(next.x) < 4.75 && FastMath.abs(next.y) < 3)
           return true;
        else return false;
    }
    
    public Node getNode()
    {
        return playerNode;
    }
    
    public Spatial getSpatial()
    {
        return mesh;
    }
    
    public Vector3f getPos() 
    {
        return position;
    }
    
    public float getAngle()
    {
        return angle;
    }
    
}
