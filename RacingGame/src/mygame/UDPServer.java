
package mygame;

import java.lang.Math;
import java.net.*;
import java.io.*;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class UDPServer{
        private static final String error = "Usage: java UDPServer <number "
            + "of players>\n";
        private static final boolean settingUp = true;

        private static PlayerData[] player;

        public static void main(String args[]){

        if(args.length != 1 || !args[0].matches("\\d*"))
        {
            System.out.println(error);
            System.exit(-1);
        }

        player = new mygame.PlayerData[Integer.parseInt(args[0])];
        for(int i = 0; i < player.length; i++){
            player[i] = new PlayerData(i);
        }
        int latestPacketReceived = 0;
        int latestPacketSent = 0;
        int packetList[] = new int[player.length];
        for(int i = 0; i < packetList.length; i++){
            packetList[i] = 0;
        }
        char comm;
        
        DatagramSocket aSocket = null;
            try{
                aSocket = new DatagramSocket(6789);
                byte[] buffer = new byte[2048];
                while(true){

                        //Setup streams
                        latestPacketSent++;
                        DatagramPacket request = new DatagramPacket(buffer, buffer.length);
                        aSocket.receive(request);
                        ByteArrayInputStream bais = new ByteArrayInputStream(buffer);
                        ObjectInputStream ois = new ObjectInputStream(bais);
                        
                        //Maintain package order and read objects
                        latestPacketReceived = ois.readInt();
                        PlayerData checkPlayer = (PlayerData) ois.readObject();
                        

                        //Maintain package order
                        if(latestPacketReceived < packetList[checkPlayer.getIndex()]){
                            System.out.println("packet from " + 
                                    checkPlayer.getIndex() + "thrown out:"
                                    + latestPacketReceived);
                            System.out.println("Because " + latestPacketReceived
                                    + " is less than " + packetList[checkPlayer.getIndex()]);
                                   
                            continue;
                        }
                        else packetList[checkPlayer.getIndex()] = latestPacketReceived;

                        //Car crash detection
                        for(int i = 0; i < player.length; i++){
                            if(checkPlayer.getIndex() != i){
                                if(Math.pow(player[i].x-checkPlayer.x, 2) +
                                    Math.pow(player[i].y - checkPlayer.y, 2) < .0125){
                                    checkPlayer.crash();
                                    player[i].crash();
                                    System.out.print("crash occured");
                                }
                            }
                        }

                        //Store info in to array
                        System.out.println(checkPlayer.x +" " + checkPlayer.y);
                        player[checkPlayer.getIndex()] = checkPlayer;

                        ois.close();
                        bais.close();
                       
                        //Reply with array depicting game state
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        ObjectOutputStream oos = new ObjectOutputStream(baos);
                        oos.writeInt(latestPacketSent);
                        oos.writeObject(player);
                        byte[] replyBytes;
                        replyBytes = baos.toByteArray();
                        oos.close();
                        baos.close();
                        DatagramPacket reply = new DatagramPacket(replyBytes,
                        replyBytes.length, request.getAddress(), request.getPort());
                        aSocket.send(reply);
                }
            }catch (SocketException e){System.out.println("Socket: " + e.getMessage());}
             catch (IOException e) {System.out.println("IO: " + e.getMessage());}
             catch (ClassNotFoundException e) {System.out.println("Error reading class:" + e.getMessage());}
        finally {if(aSocket != null) aSocket.close();}
    }
}
