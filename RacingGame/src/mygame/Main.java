package mygame;

import com.jme3.app.SimpleApplication;
import com.jme3.bounding.BoundingBox;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.control.VehicleControl;
import com.jme3.bullet.objects.VehicleWheel;
import com.jme3.input.ChaseCamera;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.renderer.Camera;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.queue.RenderQueue.ShadowMode;
import com.jme3.scene.CameraNode;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.CameraControl.ControlDirection;
import com.jme3.scene.shape.Box;
import com.jme3.texture.Texture;
import de.lessvoid.nifty.Nifty;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Random;
import mygame.GUI.ServerSetupController;

/**
 * test
 * @author Edward Crupi
 */

//This is a test for my first commit
public class Main extends SimpleApplication {
    
    private ServerSetupController serverSetupController;
    public static boolean isRunning = false;
    public int playerNumber = 2;
    private int index = 1;
    private ArrayList<OtherPlayer> playerList = new ArrayList<OtherPlayer>();
    private PlayerData[] gameState = new PlayerData[playerNumber];
    private ObjectOutputStream oos;
    private ByteArrayOutputStream baos;
    
    //private Node[] playerNode= new Node[playerNumber];
    //Create the  Camera Nodes
    //private CameraNode[] camNode = new CameraNode[playerNumber];

    //private ChaseCamera chaseCam;
    //private boolean isFollowing = false;
    
    //protected Spatial[] player = new Spatial[playerNumber];
    protected Spatial track;
    //private RigidBodyControl[]    player_phy = new RigidBodyControl[playerNumber];
    
    //private RigidBodyControl    floor_phy, track_phy;
    //private RigidBodyControl[]    wall_phy = new RigidBodyControl[4];
    
    //private BulletAppState bulletAppState;
    public static DatagramPacket reply = null;
    public DatagramSocket aSocket = null;
    private int dataID = 0;
    private static final Box    floor = new Box(5.5f, .1f, 4.25f);
    private static final Box    garden = new Box(4f, .1f, 3.5f);
    
    private ColorRGBA[] playerColor = {ColorRGBA.Blue,
                                       ColorRGBA.Green,
                                       ColorRGBA.Pink,
                                       ColorRGBA.Cyan,
                                       ColorRGBA.White
                                      };
    
    public static final Quaternion PITCH090 = 
                new Quaternion().fromAngleAxis(FastMath.PI/2,
                new Vector3f(1,0,0));
    public static final Quaternion YAW180   = 
                new Quaternion().fromAngleAxis(FastMath.PI  ,
                new Vector3f(0,1,0));
    public static final Quaternion ROLL180  = 
                new Quaternion().fromAngleAxis(FastMath.PI  ,
                new Vector3f(0,0,1));
    public static final Quaternion ROLL090  = 
            new Quaternion().fromAngleAxis(FastMath.PI/2,
            new Vector3f(0,0,1));
    
    
    public static void main(String[] args) {
        Main app = new Main();
        app.start();
    }
    
    
    @Override
    public void simpleInitApp() {
        //bulletAppState = new BulletAppState();
        //stateManager.attach(bulletAppState);
        
        
        serverSetupController = new ServerSetupController(this);
        stateManager.attach(serverSetupController);

        NiftyJmeDisplay niftyDisplay = new NiftyJmeDisplay(assetManager, inputManager, audioRenderer, guiViewPort);

        Nifty nifty = niftyDisplay.getNifty();

        guiViewPort.addProcessor(niftyDisplay);

        nifty.fromXml("Interface/startGui.xml", "start", serverSetupController);
        
        //Set lighting so materials will show
        DirectionalLight sun = new DirectionalLight();
        sun.setDirection(new Vector3f(-.1f, -.7f, -.1f));
        
        //Initialize the players
        initPlayers(playerNumber);
        
        //initReceiver();

        flyCam.setEnabled(false);
        //initCams();
        initFloor();
        initKeys();
        //flyCam.setEnabled(false);
        //initTrack();
        //initWalls();
        //addPlayerPhysics();
    };
    
    /*public void initReceiver(){
        try{
            aSocket = new DatagramSocket();
            byte [] m = comm.getBytes();
            InetAddress aHost = InetAddress.getByName("localhost");
            int serverPort = 6789;
            DatagramPacket request = new DatagramPacket(m,  comm.length(), aHost, serverPort);
            aSocket.send(request);
            byte[] buffer = new byte[1];
            reply = new DatagramPacket(buffer, buffer.length);
            aSocket.receive(reply);
       }
       catch (SocketException e){System.out.println("Socket: " + e.getMessage());}
       catch (IOException e){System.out.println("IO: " + e.getMessage());}
    }*/
    
    /*public void initCams(){

        for(int i = 0; i < playerNumber; i++)
        {
            camNode[i] = new CameraNode("Camera Node", cam);
            //This mode means that camera copies the movements of the target:
            camNode[i].setControlDir(ControlDirection.SpatialToCamera);

            playerNode[i].attachChild(camNode[i]);
        
            camNode[i].setLocalRotation(playerNode[i].getLocalRotation());
            camNode[i].move(new Vector3f(0, 2, 2));
            camNode[i].lookAt(playerNode[i].getLocalTranslation(), Vector3f.UNIT_Y);
            //camNode[i].rotate(0, 0, FastMath.PI);
            //playerNode[i].setLocalRotation(YAW180);
            //Rotate the camNode to look at the target:
            //camNode[i].lookAt(playerNode[i].getLocalTranslation(), Vector3f.UNIT_Y);
            //camNode[i].setLocalTranslation(new Vector3f(0, -2, 2));
            //camNode[i].lookAt(player[i].getLocalTranslation(), Vector3f.ZERO);
        }
    }*/
    
    public void setPlayerNumber (int n)
    {
        playerNumber = n;
    }
    
    public void initPlayers(int n) {

        //Random random = new Random();
        //index = random.nextInt(playerNumber);
        //Initialize Players
        for(int i = 0; i < n; i++)
        {
            //Add other players
            if(i != index)
            {
                playerList.add(new OtherPlayer(-5f+i*.3f, -3f, .1f, i, assetManager));
                rootNode.attachChild(playerList.get(i).getSpatial());
                continue;
            }
            //Add the player this client will be controlling
            playerList.add(new Player(-5f+i*.2f, -3f, .1f, i, assetManager, inputManager));
            rootNode.attachChild(playerList.get(i).getSpatial());
        }
    }
    
    public void initFloor() {
        Geometry floor_geo = new Geometry("Floor", floor);
        Material floor_mat = new Material(assetManager, 
                "Common/MatDefs/Misc/Unshaded.j3md");
        Texture floor_tex = assetManager.loadTexture("Textures/DS_Theme.jpg");
        floor_mat.setTexture("ColorMap", floor_tex);

        floor_geo.setMaterial(floor_mat);
        floor_geo.setLocalRotation(PITCH090);
        this.rootNode.attachChild(floor_geo);
        //floor_phy = new RigidBodyControl(0.0f);
        //floor_geo.addControl(floor_phy);
        //bulletAppState.getPhysicsSpace().add(floor_phy);
    };
    
    /*public void initWalls() {
        Geometry[] wall_geo = new Geometry[4];
        for(int i = 0; i < 4; i++)
        {
            wall_geo[i] = new Geometry("Wall", wall[i]);
            Material wall_mat = new Material(assetManager,
                    "Common/MatDefs/Misc/Unshaded.j3md");
            wall_mat.setColor("Color", ColorRGBA.Orange);
            wall_geo[i].setMaterial(wall_mat);
            wall_phy[i] = new RigidBodyControl(0.0f);
            wall_geo[i].addControl(wall_phy[i]);
            bulletAppState.getPhysicsSpace().add(wall_phy[i]);
        }
        
    };*/
    
    public void initTrack(){
        //Initialize Track
        track = assetManager.loadModel(
                "Models/untitled.obj");
        Material trackmat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        trackmat.setColor("ColorMap", ColorRGBA.White);
        track.setMaterial(trackmat);
        track.setLocalScale(50f);
        track.setLocalRotation(PITCH090);
        track.move(-7.5f, 4.5f, -15f);
        this.rootNode.attachChild(track);

    };
    
    private void initKeys()
    {
        inputManager.addMapping("Pause", new KeyTrigger(KeyInput.KEY_P));
        inputManager.addListener(actionListener, "Pause");
    }
    
    private ActionListener actionListener = new ActionListener(){
        public void onAction(String name, boolean keyPressed, float tpf){
            if(name.equals("Pause") && !keyPressed){
                isRunning = !isRunning;
            }
        }
    };
    
    private AnalogListener analogListener = new AnalogListener(){
        public void onAnalog(String name, float value, float tpf){
            if(isRunning)
            {
            }
            else {System.out.println("Press P to unpause.");
            }
        }
    };

    int latestPacketReceived = 0;
    int latestPacketSent = 0;
    
    @Override
    public void simpleUpdate(float tpf) {
        if(isRunning)
        {
            for(int i = 0; i < playerNumber; i++)
            {
                playerList.get(i).Update();
            }
            try{
                //Send Player's position over
                latestPacketSent++;
                baos = new ByteArrayOutputStream();
                oos = new ObjectOutputStream(baos);
                oos.writeInt(latestPacketSent);
                oos.writeObject(playerList.get(index).getData());
                oos.close();
                oos.flush();
                byte[] serializedPlayer = baos.toByteArray();
                //System.out.println(serializedPlayer.length);
                aSocket = new DatagramSocket();
                aSocket.setSoTimeout(200);
                InetAddress aHost = InetAddress.getByName("192.168.0.47");
                int serverPort = 6789;
                DatagramPacket request = new DatagramPacket(serializedPlayer,
                        serializedPlayer.length, aHost, serverPort);
                aSocket.send(request);
                
                //Receive game information
                byte[] buffer = new byte[2048];
                reply = new DatagramPacket(buffer, buffer.length);
                aSocket.receive(reply);
                ByteArrayInputStream bais = new ByteArrayInputStream(buffer);
                ObjectInputStream ois = new ObjectInputStream(bais);
                int packetCheck = ois.readInt();
                if(latestPacketReceived < packetCheck){
                    latestPacketReceived = packetCheck;
                    gameState = (PlayerData[]) ois.readObject();
                    
                    //Update all non players
                    for(int i = 0; i < playerNumber; i++)
                    {
                        if(i != index)
                            playerList.get(i).Update(gameState[i]);
                    }
                }
                ois.close();
                bais.close();
            
            }
            catch (SocketException e){System.out.println("Socket: " + e.getMessage());}
            catch (IOException e){System.out.println("IO: " + e.getMessage());
            e.printStackTrace();}
            catch (ClassNotFoundException e){System.out.println("Class not found: "+e.getMessage());}
            finally{if (aSocket != null) aSocket.close();}
        }
    };

    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    };
}