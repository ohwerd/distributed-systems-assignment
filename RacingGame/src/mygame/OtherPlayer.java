/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Matrix3f;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

/**
 *
 * @author edwardcrupi
 */
public class OtherPlayer implements java.io.Serializable {
    
    private Spatial mesh;
    private Node playerNode;
    //private RigidBodyControl phy;
    private Vector3f position, velocity;
    private float speed = 0, angle = 0;
    private ColorRGBA color;
    private AssetManager assetManager;
    private InputManager inputManager;
    private static final float MAXSPEED = .005f;
    public static final Quaternion PITCH090 = 
                new Quaternion().fromAngleAxis(FastMath.PI/2,
                new Vector3f(1,0,0));
    private PlayerData playerData;
    private int index;
    
    private ColorRGBA[] playerColor = {ColorRGBA.Blue,
                                   ColorRGBA.Green,
                                   ColorRGBA.Pink,
                                   ColorRGBA.Cyan,
                                   ColorRGBA.White
                                  };
    
    public OtherPlayer(float x, float y, float z, int newIndex, AssetManager newAM)
    {
        position = new Vector3f(x, y, z);
        velocity = Vector3f.ZERO;
        playerData = new PlayerData(speed, position.x, position.y, position.z, angle, newIndex);
        index = newIndex;
        color = playerColor[newIndex];
        assetManager = newAM;
        
        //Initialize Player Mesh
        mesh = assetManager.loadModel(
            "Models/newcar.j3o");
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", color);
        mesh.setMaterial(mat);

        //Get model in right shape and position.        
        mesh.scale(2f);
        mesh.setLocalRotation(PITCH090);
        mesh.rotate(0, FastMath.PI, 0);
    };

    
    public void movePlayer()
    {
        velocity = new Vector3f(
                    (float)Math.sin(angle)*speed,
                    (float)Math.cos(angle)*speed,
                    0);
        if(!blocked(position.add(velocity))){
            position = position.add(velocity);
            mesh.setLocalTranslation(position);
        }
        else speed = 0;
    };
    
    public PlayerData getData(){
        return playerData;
    }
    
    public void Update()
    {
        if(playerData.isCrashed()){
            speed = 0;
        }
        playerData.update(speed, position.x, position.y, position.z, angle);
        //Slow down if not accelerating
        if(speed > 0)
        {
            speed -= .000005;
        }
        if(speed < 0)
        {
            speed += .000005;
        }
        if(speed > MAXSPEED){
            speed = MAXSPEED;
        }
        if(speed < -MAXSPEED){
            speed = -MAXSPEED;
        }
        
        //Border collision detection
        if(position.x > 5.5f){
            position.x = 5.5f;
            speed = 0;
        }
        if(position.y < -4){

            position.y = -4f;
            speed = 0;
        }
        if(position.x < -5.5){
            position.x = -5.5f;
            speed = 0;
        }
        if(position.y > 4){
            position.y = 4f;
            speed = 0;
        }
        
        /*
        if(position.x > -3 && position.x < 3  && position.y < 2 && position.y > -2 )
        {
            if(position.x > 0 && position.y > 0)
                if(position.x > 3)
                    position.x = 3f;
                else position.y = 2f;
            else if(position.x > 0 && position.y < 0)
                if(position.x > 3)
                    position.x = 3f;
                else position.y = -2f;
            else if(position.x < 0 && position.y < 0)
                if(position.x < -3)
                    position.x = -3f;
                else position.y = -2f;
            else if(position.x < 0 && position.y > 0)
                if(position.x < -3)
                    position.x = -3f;
                else position.y = 2f;
            speed = 0;
        }*/
        //Move the player
        movePlayer();
    }
    
    public void Update(PlayerData data){
            position = new Vector3f(data.x, data.y, data.z);
            angle = data.rotation;
            speed = data.speed;
            Quaternion rotate = PITCH090.mult(new Quaternion().fromAngleAxis(-angle, new Vector3f(0,1,0)));
            mesh.setLocalRotation(rotate);
            mesh.rotate(0, FastMath.PI, 0);
    }

    public boolean blocked(Vector3f next){
        if(FastMath.abs(next.x) < 4.75 && FastMath.abs(next.y) < 3)
           return true;
        else return false;
    }
    
    public Node getNode()
    {
        return playerNode;
    }
    
    public Spatial getSpatial()
    {
        return mesh;
    }
    
    public Vector3f getPos() 
    {
        return position;
    }
    
    public float getAngle()
    {
        return angle;
    }
    
}
